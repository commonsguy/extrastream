This project exists in support of [https://issuetracker.google.com/issues/176280829](https://issuetracker.google.com/issues/176280829),
to illustrate the problem with using `EXTRA_STREAM` alone, without `setClipData()`,
for `ACTION_SEND`.
