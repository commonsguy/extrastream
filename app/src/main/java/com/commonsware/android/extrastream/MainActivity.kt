/*
  Copyright (c) 2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.
*/

package com.commonsware.android.extrastream

import android.content.ClipData
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ShareCompat
import androidx.lifecycle.lifecycleScope
import com.commonsware.android.extrastream.databinding.ActivityMainBinding
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach


class MainActivity : AppCompatActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    val binding = ActivityMainBinding.inflate(layoutInflater)

    setContentView(binding.root)

    val motor: MainMotor by viewModels()
    val allButtons =
      listOf(binding.extra, binding.clipData, binding.shareCompat)

    motor.results.onEach { state ->
      when (state) {
        MainMotor.ViewState.Loading -> {
          allButtons.forEach { it.isEnabled = false }
          binding.error.text = ""
        }
        is MainMotor.ViewState.Content -> {
          allButtons.forEach { it.isEnabled = true }
          binding.extra.setOnClickListener { tryExtraStream(state.image) }
          binding.clipData.setOnClickListener { tryClipData(state.image) }
          binding.shareCompat.setOnClickListener { tryShareCompat(state.image) }
          binding.error.text = ""
        }
        MainMotor.ViewState.Error -> {
          allButtons.forEach { it.isEnabled = false }
          binding.error.text = "Ick! Something went wrong!"
        }
      }
    }.launchIn(lifecycleScope)

    motor.prepImage()
  }

  private fun tryExtraStream(uri: Uri) {
    val intent = Intent(Intent.ACTION_SEND).apply {
      putExtra(Intent.EXTRA_STREAM, uri)
      type = "image/webp"
      addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
    }

    startActivity(Intent.createChooser(intent, null))
  }

  private fun tryClipData(uri: Uri) {
    val intent = Intent(Intent.ACTION_SEND).apply {
      clipData = ClipData.newRawUri(null, uri)
      putExtra(Intent.EXTRA_STREAM, uri)
      type = "image/webp"
      addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
    }

    startActivity(Intent.createChooser(intent, null))
  }

  private fun tryShareCompat(uri: Uri) {
    ShareCompat.IntentBuilder.from(this)
      .setType("image/webp")
      .addStream(uri)
      .startChooser()
  }
}