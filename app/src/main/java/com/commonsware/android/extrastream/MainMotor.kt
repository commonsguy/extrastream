/*
  Copyright (c) 2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.
*/

package com.commonsware.android.extrastream

import android.app.Application
import android.net.Uri
import android.util.Log
import androidx.core.content.FileProvider
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileOutputStream

private const val AUTHORITY = "${BuildConfig.APPLICATION_ID}.provider"

class MainMotor(private val app: Application) : AndroidViewModel(app) {
  sealed class ViewState {
    object Loading : ViewState()
    data class Content(val image: Uri) : ViewState()
    object Error : ViewState()
  }

  private val _results = MutableStateFlow<ViewState>(ViewState.Loading)
  val results: Flow<ViewState> = _results

  fun prepImage() {
    _results.value = ViewState.Loading

    viewModelScope.launch(Dispatchers.IO) {
      try {
        val imageFile = File(app.cacheDir, "test.webp")

        if (!imageFile.exists()) {
          app.assets.open("FreedomTower-Morning.webp").use { webp ->
            FileOutputStream(imageFile).use { webp.copyTo(it) }
          }
        }

        val uri = FileProvider.getUriForFile(app, AUTHORITY, imageFile)

        _results.value = ViewState.Content(uri)
      } catch (t: Throwable) {
        Log.e("ExtraStream", "Exception copying asset", t)
        _results.value = ViewState.Error
      }
    }
  }
}